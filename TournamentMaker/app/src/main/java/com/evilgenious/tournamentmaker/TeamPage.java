package com.evilgenious.tournamentmaker;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class TeamPage extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team_page);
    }
    private void popUpWarning() {

        AlertDialog.Builder helpBuilder = new AlertDialog.Builder(this);
        helpBuilder.setTitle("Warning!!!");
        helpBuilder.setMessage("ARE YOU SURE???");
        helpBuilder.setPositiveButton("YES",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
//close the dialog
                    }
                });

        helpBuilder.setNegativeButton("NO",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

        // Remember, create doesn't show the dialog
        AlertDialog helpDialog = helpBuilder.create();
        helpDialog.show();

    }

    private void popUpInfo() {

        AlertDialog.Builder helpBuilder = new AlertDialog.Builder(this);
        helpBuilder.setTitle("Info");
        helpBuilder.setMessage("If you choose to delete the team, the team will never RESURRECT itself.");
        helpBuilder.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        // close the dialog
                    }
                });

        // Remember, create doesn't show the dialog
        AlertDialog helpDialog = helpBuilder.create();
        helpDialog.show();
    }
    //what to do when info button is pressed
    public void infoButtonClick(View view){
        Button infoButton = (Button) findViewById(R.id.infoButton);
        View.OnClickListener myListener1 = new View.OnClickListener(){
            public void onClick(View v){
                popUpInfo();
            }

        };
        infoButton.setOnClickListener(myListener1);
    }

    //what to do when login button is pressed
    public void showStatsButtonClick(View view){
        startActivity(new Intent(TeamPage.this, TeamStats.class));
    }
    //what to do when add player buttons is clicked
    public void addPlayerButtonClick(View view){
        startActivity(new Intent(TeamPage.this, TeamStats.class));
    }

    //what to do when save button is clicked
    public void saveButtonClick(View view){

    }
    //what to do when cancel button is clicked
    public void cancelButtonClick(View view){

    }
    //what to do when delete team button is cli
    public void deleteTeamButtonClick(View view) {
        // 1. Get a reference to the button.
        Button deleteTeamButton = (Button) findViewById(R.id.deleteTeamButton);
        // 2. Set the click listener to run the code.
        View.OnClickListener myListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popUpWarning();
            }
        };
        deleteTeamButton.setOnClickListener(myListener);
    }
}
