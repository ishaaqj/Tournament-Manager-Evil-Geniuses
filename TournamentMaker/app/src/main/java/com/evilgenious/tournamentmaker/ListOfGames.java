package com.evilgenious.tournamentmaker;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ListOfGames extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_of_games);
    }
    private void popUpInfo() {

        AlertDialog.Builder helpBuilder = new AlertDialog.Builder(this);
        helpBuilder.setTitle("Info");
        helpBuilder.setMessage("Here shows list of games in different rounds");
        helpBuilder.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        // close the dialog
                    }
                });

        // Remember, create doesn't show the dialog
        AlertDialog helpDialog = helpBuilder.create();
        helpDialog.show();
    }
    //onclick method
    public void infoButtonClick(View view){
        Button infoButton = (Button) findViewById(R.id.infoButton);
        View.OnClickListener myListener1 = new View.OnClickListener(){
            public void onClick(View v){
                popUpInfo();
            }
        };
        infoButton.setOnClickListener(myListener1);
    }

    //what to do when delete team button is pressed
    public void gameButtonClick(View view){
        startActivity(new Intent(ListOfGames.this, GameStats.class));    }
}
