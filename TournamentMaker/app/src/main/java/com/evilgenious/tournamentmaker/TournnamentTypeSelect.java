package com.evilgenious.tournamentmaker;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class TournnamentTypeSelect extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tournnament_type_select);
    }
    private void popUpInfo() {

        AlertDialog.Builder helpBuilder = new AlertDialog.Builder(this);
        helpBuilder.setTitle("Info");
        helpBuilder.setMessage("You select a typr for your tournament here.");
        helpBuilder.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        // close the dialog
                    }
                });

        // Remember, create doesn't show the dialog
        AlertDialog helpDialog = helpBuilder.create();
        helpDialog.show();
    }
    //onclick method
    public void infoButtonClick(View view){
        Button infoButton = (Button) findViewById(R.id.infoButton);
        View.OnClickListener myListener1 = new View.OnClickListener(){
            public void onClick(View v){
                popUpInfo();
            }
        };
        infoButton.setOnClickListener(myListener1);
    }

    //what to do when login button is pressed
    public void roundRobinButtonClick(View view){
        startActivity(new Intent(TournnamentTypeSelect.this, EnterTeams.class));
    }

    //what to do when login button is pressed
    public void knockoutButtonClick(View view){
        startActivity(new Intent(TournnamentTypeSelect.this, EnterTeams.class));
    }

    //what to do when login button is pressed
    public void combinationButtonClick(View view){
        startActivity(new Intent(TournnamentTypeSelect.this, EnterTeams.class));
    }

}
