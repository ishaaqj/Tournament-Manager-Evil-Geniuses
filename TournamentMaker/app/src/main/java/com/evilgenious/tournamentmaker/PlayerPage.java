package com.evilgenious.tournamentmaker;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class PlayerPage extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player_page);
    }

    private void popUpInfo() {

        AlertDialog.Builder helpBuilder = new AlertDialog.Builder(this);
        helpBuilder.setTitle("Info");
        helpBuilder.setMessage("You can edit profile of a player here.");
        helpBuilder.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        // close the dialog
                    }
                });

        // Remember, create doesn't show the dialog
        AlertDialog helpDialog = helpBuilder.create();
        helpDialog.show();
    }

    private void popUpWarning() {

        AlertDialog.Builder helpBuilder = new AlertDialog.Builder(this);
        helpBuilder.setTitle("Warning!!!");
        helpBuilder.setMessage("ARE YOU SURE???");
        helpBuilder.setPositiveButton("YES",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
//close the dialog
                    }
                });

        helpBuilder.setNegativeButton("NO",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
    }

    //onclick method
    public void infoButtonClick(View view) {
        Button infoButton = (Button) findViewById(R.id.infoButton);
        View.OnClickListener myListener1 = new View.OnClickListener() {
            public void onClick(View v) {
                popUpInfo();
            }
        };
        infoButton.setOnClickListener(myListener1);
    }

    private void deletePlayerButtonClick(View view) {
        // 1. Get a reference to the button.
        Button deletePlayerButton = (Button) findViewById(R.id.deletePlayerButton);
        // 2. Set the click listener to run the code.
        View.OnClickListener myListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popUpWarning();
            }
        };
        deletePlayerButton.setOnClickListener(myListener);
    }
}
